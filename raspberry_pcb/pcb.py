import math
import threading
from concurrent.futures import Future
from dataclasses import dataclass
from typing import List, Optional, Tuple

from .controllers import GroupController, Nema17GPIOMotor
from .excpetions import NotCalibratedException, NotEnabledException
from .motors_cpp.events import EventHandler, GPIOSwitchEvent, GPIOSwitchEventNotifier, TimeEvent, TimeEventNotifier
from .motors_cpp.gpio import RaspberryGPIO
from .motors_cpp.updater import Updater


@dataclass(frozen=True)
class PCBState:
    pos: Tuple[float, float]
    vel: Tuple[float, float]


@dataclass(frozen=True)
class PCBInfo:
    max_speed: Tuple[float, float]
    max_pos: Tuple[float, float]


class RaspberryPCB(threading.Thread):
    MAX_VEL_K = 0.98
    RAD_PER_M = 196.34954
    BORDER_SIZE_M = 0.018
    MAX_POS_M = (0.340, 0.231)

    SWITCH_X1_PIN = 13
    SWITCH_X2_PIN = 6
    SWITCH_Y_PIN = 5

    _future: Optional[Tuple[Future[bool], int]] = None
    _lock: threading.Lock
    _updater: Updater
    _working: bool = False

    _gpio: RaspberryGPIO
    _event_handler: EventHandler
    _planner_timer: TimeEventNotifier
    _group_controller: GroupController

    _motors: List[Nema17GPIOMotor]
    _motors_shift: List[float]
    _info: PCBInfo

    _is_enabled: bool = True
    _calibration_future: Optional[Future[bool]] = None
    _calibration_step: int = -1
    _is_calibrated: bool = False

    @property
    def info(self) -> PCBInfo:
        return self._info

    @property
    def is_enabled(self) -> bool:
        return self._is_enabled

    @property
    def is_calibrated(self) -> bool:
        return self._is_calibrated

    @property
    def cur_state(self) -> PCBState:
        state = [m.cur_state for m in self._motors]
        pos_vel = [
            ((sum(s.pos) + sh) / m.steps_per_radian, s.vel / m.steps_per_radian)
            for s, m, sh in zip(state, self._motors, self._motors_shift)
        ]
        pos = ((pos_vel[0][0] + pos_vel[1][0]) / 2 / self.RAD_PER_M, pos_vel[2][0] / self.RAD_PER_M)
        vel = ((pos_vel[0][1] + pos_vel[1][1]) / 2 / self.RAD_PER_M, pos_vel[2][1] / self.RAD_PER_M)
        return PCBState(pos, vel)

    def __init__(self) -> None:
        super().__init__()

        self._lock = threading.Lock()

        self._gpio = RaspberryGPIO()
        self._gpio.pinMode(10, RaspberryGPIO.OUTPUT)
        self.enable(False)

        motorX1 = Nema17GPIOMotor(self._gpio, 22, 27)
        motorX2 = Nema17GPIOMotor(self._gpio, 3, 2)
        motorY = Nema17GPIOMotor(self._gpio, 17, 4, inverse=True)
        self._motors = [motorX1, motorX2, motorY]
        self._motors_shift = [0, 0, 0]
        max_x_vel_accel = (
            min(motorX1.max_vel / motorX1.steps_per_radian, motorX2.max_vel / motorX2.steps_per_radian)
            * self.MAX_VEL_K,
            min(motorX1.max_accel, motorX2.max_accel),
        )
        max_y_vel_accel = (motorY.max_vel / motorY.steps_per_radian * self.MAX_VEL_K, motorY.max_accel)
        max_vel_accel = [
            (max_x_vel_accel[0] * motorX1.steps_per_radian, max_x_vel_accel[1]),
            (max_x_vel_accel[0] * motorX2.steps_per_radian, max_x_vel_accel[1]),
            (max_y_vel_accel[0] * motorY.steps_per_radian, max_x_vel_accel[1]),
        ]
        self._group_controller = GroupController([motorX1, motorX2, motorY], max_vel_accel)

        self._event_handler = EventHandler()
        switchX1 = GPIOSwitchEventNotifier(self._event_handler, self._gpio, self.SWITCH_X1_PIN, 1_000_000)
        switchX2 = GPIOSwitchEventNotifier(self._event_handler, self._gpio, self.SWITCH_X2_PIN, 1_000_000)
        switchY = GPIOSwitchEventNotifier(self._event_handler, self._gpio, self.SWITCH_Y_PIN, 1_000_000)
        self._planner_timer = TimeEventNotifier(self._event_handler, 0)

        self._updater = Updater()
        self._updater.add(motorY)
        self._updater.add(motorX1)
        self._updater.add(motorX2)
        self._updater.add(switchY)
        self._updater.add(switchX1)
        self._updater.add(switchX2)
        self._updater.add(self._planner_timer)

        self._info = PCBInfo(
            max_speed=(max_x_vel_accel[0] / self.RAD_PER_M, max_y_vel_accel[0] / self.RAD_PER_M),
            max_pos=self.MAX_POS_M,
        )

    def __del__(self) -> None:
        self.stop()

    def start(self) -> None:
        if self._working:
            return
        self._working = True
        super().start()

    def stop(self) -> None:
        if not self._working:
            return
        self._working = False

    def enable(self, enable: bool) -> None:
        if self._is_enabled == enable:
            return
        self._gpio.digitalWrite(10, int(not enable))
        self._is_enabled = enable

        if not enable:
            self._is_calibrated = False
            with self._lock:
                old_future = self._future
                self._future = None

            if old_future:
                old_future[0].set_result(False)

    def _set_new_future(self, end_time_ns: int) -> Future[bool]:
        new_future: Future[bool] = Future()

        with self._lock:
            old_future = self._future
            self._future = (new_future, end_time_ns)
        self._planner_timer.set_notify_time_ns(end_time_ns)

        if old_future:
            old_future[0].set_result(False)

        return new_future

    def _assert_enabled(self) -> None:
        if not self._is_enabled:
            raise NotEnabledException("PCB is not enabled")

    def _assert_calibrated(self) -> None:
        if not self._is_calibrated:
            raise NotCalibratedException("PCB is not calibrated")
        if self._calibration_step != -1:
            raise NotCalibratedException("PCB is is calibration process")

    def _convert_vel(self, vel: Tuple[float, float]) -> List[float]:
        return [v * self.RAD_PER_M * m.steps_per_radian for v, m in zip([vel[0], vel[0], vel[1]], self._motors)]

    def _convert_pos(self, pos: Tuple[float, float]) -> List[Tuple[int, float]]:
        target_pos = [
            p * self.RAD_PER_M * m.steps_per_radian - sh
            for p, m, sh in zip([pos[0], pos[0], pos[1]], self._motors, self._motors_shift)
        ]
        target_pos_int = [math.floor(p) for p in target_pos]
        assert len(target_pos_int) == len(self._motors)
        return [(pi, p - pi) for p, pi in zip(target_pos, target_pos_int)]

    def _do_next_callibration_step(self) -> None:
        self._calibration_step += 1
        if self._calibration_step == 0:
            self._group_controller.set_pos_limits([None] * len(self._motors))
            self._set_new_future(self._group_controller.const_vel([-10000, -10000, -10000], 1000))
        elif self._calibration_step == 1:
            future = self._set_new_future(self._group_controller.const_vel([10000, 10000, 10000], 0.1))
            future.add_done_callback(lambda _: self._do_next_callibration_step())
        elif self._calibration_step == 2:
            self._set_new_future(self._group_controller.const_vel([-500, -500, -500], 1000))
        elif self._calibration_step == 3:
            self._is_calibrated = True
            radian_shift = self.BORDER_SIZE_M * self.RAD_PER_M
            self._motors_shift = [-sum(m.cur_state.pos) - radian_shift * m.steps_per_radian for m in self._motors]
            self._group_controller.set_pos_limits(
                [
                    (p1[0], p2[0])
                    for p1, p2 in zip(self._convert_pos((-0.005, -0.005)), self._convert_pos(self.MAX_POS_M))
                ]
            )
            end_time_ns = self._group_controller.p2p(self._convert_pos((0, 0)), [0, 0, 0])
            future = self._set_new_future(end_time_ns)
            future.add_done_callback(lambda _: self._do_next_callibration_step())
        elif self._calibration_step == 4:
            assert self._calibration_future is not None
            self._calibration_step = -1
            self._calibration_future.set_result(True)

    def calibrate(self) -> Future[bool]:
        self._assert_enabled()
        if self._calibration_step != -1:
            assert self._calibration_future is not None
            return self._calibration_future
        self._calibration_future = Future()
        self._do_next_callibration_step()
        return self._calibration_future

    def const_vel(self, vel: Tuple[float, float], duration: float) -> Future[bool]:
        self._assert_enabled()
        self._assert_calibrated()
        end_time_ns = self._group_controller.const_vel(self._convert_vel(vel), duration)
        return self._set_new_future(end_time_ns)

    def stop_motors(self, sleep_for: float = 0) -> Future[bool]:
        self._assert_enabled()
        end_time_ns = self._group_controller.stop_motors(sleep_for)
        return self._set_new_future(end_time_ns)

    def p2p(self, pos: Tuple[float, float], vel: Tuple[float, float]) -> Future[bool]:
        self._assert_enabled()
        self._assert_calibrated()
        end_time_ns = self._group_controller.p2p(self._convert_pos(pos), self._convert_vel(vel))
        return self._set_new_future(end_time_ns)

    def _check_and_complete_future(self, event_time_ns: int) -> None:
        if (self._future is None) or (event_time_ns < self._future[1]):
            return

        with self._lock:
            if (self._future is None) or (event_time_ns < self._future[1]):
                return
            old_future = self._future[0]
            self._future = None

        old_future.set_result(True)

    def run(self) -> None:
        self._updater.start(0)

        stopped_motors = set()

        while self._working:
            event = self._event_handler.wait(0.1)
            if event is None:
                continue
            if isinstance(event, TimeEvent) and (event.timer_id == self._planner_timer.timer_id):
                self._check_and_complete_future(event.event_time_ns)

            if isinstance(event, GPIOSwitchEvent) and self._calibration_step in (0, 2) and event.value == 0:
                if event.pin == self.SWITCH_X1_PIN:
                    self._group_controller.stop_single_motor(0)
                    stopped_motors.add(0)
                elif event.pin == self.SWITCH_X2_PIN:
                    self._group_controller.stop_single_motor(1)
                    stopped_motors.add(1)
                elif event.pin == self.SWITCH_Y_PIN:
                    self._group_controller.stop_single_motor(2)
                    stopped_motors.add(2)
                if len(stopped_motors) == 3:
                    stopped_motors.clear()
                    self._do_next_callibration_step()

        self._updater.stop()
