class PCBException(Exception):
    pass


class NotEnabledException(PCBException):
    pass


class NotCalibratedException(PCBException):
    pass


class PCBServerException(Exception):
    pass


class BadRequestException(PCBServerException):
    pass
