from typing import List

from raspberry_pcb.motors_cpp.planner import GroupPlanner, Planner, SimpleConstPlanner, SimplePlanner


class StopPlanner(GroupPlanner):
    """Планер торможения с заданным ускорением.

    Планер выполняет торможение после прошествия `sleep_for` секунд со скорости `v_start` до
    полной остановки с ускорением `accel`.
    """

    def __init__(self, v_start: float, accel: float, sleep_for: float = 0) -> None:
        if accel <= 0:
            raise ValueError("accel must be grater than 0")
        if sleep_for < 0:
            raise ValueError("sleep_for must be grater than or equal to 0")

        planners: List[Planner] = []
        if v_start != 0:
            if sleep_for > 0:
                planners.append(SimpleConstPlanner(v_start, sleep_for))
            planners.append(SimplePlanner(v_start, 0, abs(v_start) / accel))
        else:
            planners.append(SimpleConstPlanner(0, 0))
        super().__init__(planners)
