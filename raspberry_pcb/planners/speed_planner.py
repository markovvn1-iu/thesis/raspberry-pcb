from typing import List

from raspberry_pcb.motors_cpp.planner import GroupPlanner, Planner, SimpleConstPlanner, SimplePlanner


class SpeedPlanner(GroupPlanner):
    """Планер с ускорением до заданной скорости и движением с ней указанное количество времени.

    Планер за время `duration` равномерно изменяет скорость от `v_start` до `v_end` с ускорением
    `a_max` и после двигается с постоянной скоростью `v_end`.
    Если `duration` меньше чем время, требующееся на разгон, то сколько во время окончания движения
    будет не равна `v_end`.
    """

    def __init__(self, v_start: float, v_end: float, duration: float, a_max: float) -> None:
        if duration <= 0:
            raise ValueError("duration cannot be less than or equeal to 0")
        if a_max <= 0:
            raise ValueError("a_max cannot be less than or equeal to 0")

        accel_t = abs(v_end - v_start) / a_max
        if accel_t > duration:
            new_v_end = (v_end - v_start) / accel_t * duration + v_start
            super().__init__([SimplePlanner(v_start, new_v_end, duration)])
        else:
            res: List[Planner] = []
            if accel_t > 0:
                res.append(SimplePlanner(v_start, v_end, accel_t))
            res.append(SimpleConstPlanner(v_end, duration - accel_t))
            super().__init__(res)
