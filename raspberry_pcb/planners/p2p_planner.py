import math
from typing import Callable, List, Optional, Tuple

from raspberry_pcb.motors_cpp.planner import GroupPlanner, Planner, SimpleConstPlanner, SimplePlanner


def _calc_y_case2(  # pylint: disable=too-many-arguments
    v_s: float, v_e: float, s: float, v_m: float, a: float, sign: int
) -> Callable[[float], Tuple[float, float]]:
    assert v_m >= v_s >= v_e >= 0, "It is not the case2"
    s1, s2, s3 = s * a, (v_s * v_s + v_e * v_e) / 2, (v_s * v_s - v_e * v_e) / 2
    assert s1 >= s2, "It is not the case2"

    y_sing_2 = s1 + s2
    if v_m * v_m < y_sing_2:
        # | ____
        # |/    \
        # *      \
        # |       *
        # +--------------
        y, y_inv = v_m, y_sing_2 / v_m
    else:
        # |
        # |/\
        # *  \
        # |   *
        # +--------------
        y = math.sqrt(y_sing_2)
        y_inv = y

    t_min = (y_inv + y - v_s - v_e) / a  # минималььное время выполнения всего плана
    t_crit1 = max(0, ((s1 + s2) / v_s - v_e) / a - t_min) if v_s > 0 else None
    t_crit2 = max(t_crit1 or 0, ((s1 - s2) / v_e + v_s) / a - t_min) if v_e > 0 else None
    k1, k2 = s - s3 / a, t_min - (v_s - v_e) / a
    # assert (t_crit1 is None) or (0 <= t_crit1)
    # assert (t_crit2 is None) or ((t_crit1 is not None) and (t_crit1 <= t_crit2))

    # (0 <= t <= t_crit1) => (v_m >= y >= v_s >= v_e >= 0)
    # (t_crit1 <= t <= t_crit2) => (v_m >= v_s >= y >= v_e >= 0)
    # (t_crit2 <= t) => (v_m >= v_s >= v_e >= y >= 0)

    def wrap(t: float) -> Tuple[float, float]:
        if t < 0:
            raise ValueError("t must be grater than or equal to 0")
        if t == 0:
            y1 = y
        elif (t_crit2 is not None) and (t > t_crit2):
            # v_m >= v_s >= v_e > y >= 0
            b = (v_s + v_e - (t_min + t) * a) / 2
            y1 = b + math.sqrt(b * b + s1 - s2)
        elif (t_crit1 is not None) and (t > t_crit1):
            # v_m >= v_s > y >= v_e >= 0
            y1 = k1 / (k2 + t)
        else:
            # v_m >= y >= v_s >= v_e >= 0
            b = (v_s + v_e + (t_min + t) * a) / 2
            y1 = b - math.sqrt(b * b - s1 - s2)

        yt = t_min + t - (abs(v_s - y1) + abs(v_e - y1)) / a
        return sign * y1, yt

    return wrap


def _calc_y(
    v_start: float, p_end: float, v_end: float, max_vel: float, max_accel: float
) -> Callable[[float], Tuple[float, float]]:
    """Calculate y.
    t - how much time we need to add to the minimum time.
    """
    inverse = v_start < 0
    if inverse:
        v_start, p_end, v_end = -v_start, -p_end, -v_end

    inverse_sign = -1 if inverse else 1

    a = max_accel
    s1 = v_start * v_start / 2 / a
    s2 = v_end * v_end / 2 / a

    if v_end >= 0:
        s3 = p_end - s1 - s2
        if s3 < 0:
            if p_end > s1:
                # Особый случай, когда существует второе решение, которое требует меньше времени, однако
                # ограничено максимальным временем выполнения, из-за чего не может быть растянуто на бесконечно
                # большой промежуток времени.
                pass
            return _calc_y_case2(0, 0, -s3, max_vel, max_accel, -inverse_sign)  # case 1
        return _calc_y_case2(
            max(v_start, v_end), min(v_start, v_end), p_end, max_vel, max_accel, inverse_sign
        )  # case 2

    s3 = p_end - s1 + s2
    if s3 < 0:
        return _calc_y_case2(-v_end, 0, -p_end + s1, max_vel, max_accel, -inverse_sign)  # case 3
    return _calc_y_case2(v_start, 0, p_end + s2, max_vel, max_accel, inverse_sign)  # case 4


def _build_planner(v_start: float, v_end: float, y: float, ty: float, max_accel: float) -> List[Planner]:
    res: List[Planner] = []
    if v_start != y:
        res.append(SimplePlanner(v_start, y, abs(v_start - y) / max_accel))
    if ty > 0:
        res.append(SimpleConstPlanner(y, ty))
    if v_end != y:
        res.append(SimplePlanner(y, v_end, abs(v_end - y) / max_accel))
    if len(res) == 0:
        res.append(SimpleConstPlanner(v_start, 0))
    return res


class P2PPlanner(GroupPlanner):
    """Планер от точки до точки.

    Планер выполняет плавное движение до заданной точки `p_end`. Движение начинается
    со скоростью `v_start` и заканчивается со скоростью `v_end`, при этом на всем пути
    скорость движения не превышает `max_vel`, а ускорение не превышает `max_accel`.
    """

    _build_planner: Optional[Callable[[float], List[Planner]]] = None

    def __init__(  # pylint: disable=too-many-arguments
        self, v_start: float, p_end: float, v_end: float, max_vel: float, max_accel: float
    ) -> None:
        if max_vel <= 0:
            raise ValueError("max_vel must be grater than 0")
        if max_accel <= 0:
            raise ValueError("max_accel must be grater than 0")
        if abs(v_start) > max_vel:
            raise ValueError("v_end must be less than max_vel")
        if abs(v_end) > max_vel:
            raise ValueError("v_end must be less than max_vel")

        calc_y = _calc_y(v_start, p_end, v_end, max_vel, max_accel)
        self._build_planner = lambda t: _build_planner(v_start, v_end, *calc_y(t), max_accel)
        super().__init__(self._build_planner(0))

    def add_time(self, dt: float) -> Planner:
        assert self._build_planner is not None
        return GroupPlanner(self._build_planner(dt))
