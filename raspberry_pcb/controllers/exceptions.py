class ControllerException(Exception):
    pass


class PosOutOfRangeException(ControllerException):
    pass
