import math

from raspberry_pcb.motors_cpp import cpp_gpio, cpp_planner, cpp_step_motor, cpp_updater, motors_cpp

from .motor import PlannerMotor


class Nema17GPIOMotor(PlannerMotor, cpp_updater.IUpdate):
    _STEPS_PER_ROT = 200 * 32
    _STEPS_PER_RAD: float = _STEPS_PER_ROT / (2 * math.pi)

    _motor: cpp_step_motor.SoftwareStepMotor
    _controller: cpp_gpio.SoftwareStepMotorGPIOController
    _planner: cpp_planner.PlannerMotorController

    @property
    def _base(self) -> motors_cpp.IUpdate:
        return self._planner._base

    @property
    def max_vel(self) -> float:
        return 60000

    @property
    def max_accel(self) -> float:
        return 675000

    @property
    def steps_per_radian(self) -> float:
        return self._STEPS_PER_RAD

    @property
    def cur_state(self) -> cpp_step_motor.MotorState:
        return self._motor.cur_state

    def __init__(self, gpio: cpp_gpio.RaspberryGPIO, dir_pin: int, step_pin: int, inverse: bool = False) -> None:
        self._controller = cpp_gpio.SoftwareStepMotorGPIOController(gpio, dir_pin, step_pin, inverse)
        self._motor = cpp_step_motor.SoftwareStepMotor(self._controller, self.max_vel, kp=1)
        self._planner = cpp_planner.PlannerMotorController(self._motor)

    def set_planner(self, planner: cpp_planner.CompiledPlanner) -> None:
        self._planner.set_planner(planner)
