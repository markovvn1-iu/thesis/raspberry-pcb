from typing import List, Optional, Tuple

from raspberry_pcb.planners import CompiledPlanner, GroupPlanner, P2PPlanner, Planner, SpeedPlanner, StopPlanner

from .exceptions import PosOutOfRangeException
from .motor import MotorState, PlannerMotor


def _clip(value: float, max_value: float) -> float:
    return max(-max_value, min(value, max_value))


class GroupController:
    STOP_IF_NO_PLANNER_AFTER = 0.1  # sec

    _motors: List[PlannerMotor]
    _pos_limits: List[Optional[Tuple[int, int]]]
    _max_vel_accel: List[Tuple[float, float]]

    def __init__(self, motors: List[PlannerMotor], max_vel_accel: List[Tuple[float, float]]) -> None:
        if len(motors) != len(max_vel_accel):
            raise ValueError("Incorrect length of the max_vel_accel")

        self._motors = motors
        self._max_vel_accel = max_vel_accel
        self._pos_limits = [None] * len(motors)

    def set_pos_limits(self, limits: List[Optional[Tuple[int, int]]]) -> None:
        if len(limits) != len(self._motors):
            raise ValueError("Incorrect length of the limits")
        self._pos_limits = limits

    def _set_planners(self, planners: List[Tuple[Planner, MotorState]], ignore_limits: bool = False) -> int:
        compiled_planners: List[CompiledPlanner] = []
        end_time_ns = max(round(p[0].info.duration * 1e9) + p[1].timestamp_ns for p in planners)

        for (planner, state), (_, max_accel), pos_limits in zip(planners, self._max_vel_accel, self._pos_limits):
            if planner.info.end_vel != 0:
                stop_planner = StopPlanner(planner.info.end_vel, max_accel, self.STOP_IF_NO_PLANNER_AFTER)
                planner = GroupPlanner([planner, stop_planner])

            compiled_planner = planner.compile(state.timestamp_ns, state.pos)
            end_pos = compiled_planner.info.end_pos[0]
            if (not ignore_limits) and (pos_limits is not None) and (not pos_limits[0] <= end_pos <= pos_limits[1]):
                raise PosOutOfRangeException("End position for one of the planner is out of range")
            compiled_planners.append(compiled_planner)

        for m, compiled_planner in zip(self._motors, compiled_planners):
            m.set_planner(compiled_planner)

        return end_time_ns

    def const_vel(self, vel: List[float], duration: float) -> int:
        if len(self._motors) != len(vel):
            raise ValueError("Incorrect length of the vel")
        if duration <= 0:
            raise ValueError("duration must be grater than 0")

        planners: List[Tuple[Planner, MotorState]] = []
        for m, v, (max_vel, max_accel) in zip(self._motors, vel, self._max_vel_accel):
            state = m.cur_state
            planner = SpeedPlanner(_clip(state.vel, max_vel), _clip(v, max_vel), duration, max_accel)
            planners.append((planner, state))

        return self._set_planners(planners)

    def stop_motors(self, sleep_for: float = 0) -> int:
        planners: List[Tuple[Planner, MotorState]] = []
        for m, (max_vel, max_accel) in zip(self._motors, self._max_vel_accel):
            state = m.cur_state
            planner = StopPlanner(_clip(state.vel, max_vel), max_accel, sleep_for)
            planners.append((planner, state))

        return self._set_planners(planners, ignore_limits=True)

    def stop_single_motor(self, i: int) -> int:
        m = self._motors[i]
        state = m.cur_state
        planner = StopPlanner(state.vel, m.max_accel).compile(state.timestamp_ns, state.pos)
        m.set_planner(planner)
        return planner.info.end_time_ns

    def p2p(self, pos: List[Tuple[int, float]], vel: List[float]) -> int:
        if len(pos) != len(self._motors):
            raise ValueError("Incorrect length of the pos")
        if len(vel) != len(self._motors):
            raise ValueError("Incorrect length of the vel")

        raw_planners: List[Tuple[MotorState, P2PPlanner]] = []
        for m, p_end, v_end, (max_vel, max_accel) in zip(self._motors, pos, vel, self._max_vel_accel):
            state = m.cur_state
            v_start, v_end = _clip(state.vel, max_vel), _clip(v_end, max_vel)
            delta_p = (p_end[0] - state.pos[0]) + (p_end[1] - state.pos[1])
            planner = P2PPlanner(v_start, delta_p, v_end, max_vel, max_accel)
            raw_planners.append((state, planner))

        min_timestamp_ns = min(s.timestamp_ns for s, p in raw_planners)
        planner_duration = [(s.timestamp_ns - min_timestamp_ns) / 1e9 + p.info.duration for s, p in raw_planners]
        total_duration = max(planner_duration)

        planners: List[Tuple[Planner, MotorState]] = []
        for (state, raw_planner), duration in zip(raw_planners, planner_duration):
            planner_ = raw_planner.add_time(total_duration - duration)
            planners.append((planner_, state))

        return self._set_planners(planners)
