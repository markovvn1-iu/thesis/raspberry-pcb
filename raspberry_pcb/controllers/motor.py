from abc import ABC, abstractmethod

from raspberry_pcb.motors_cpp import cpp_planner
from raspberry_pcb.motors_cpp.step_motor import MotorState


class PlannerMotor(ABC):
    @property
    @abstractmethod
    def max_vel(self) -> float:
        """Max motor velocity in steps per second."""

    @property
    @abstractmethod
    def max_accel(self) -> float:
        """Max motor acceleration in steps per second square."""

    @property
    @abstractmethod
    def steps_per_radian(self) -> float:
        """How many steps in one radian."""

    @property
    @abstractmethod
    def cur_state(self) -> MotorState:
        """Current motor state (in steps)."""

    @abstractmethod
    def set_planner(self, planner: cpp_planner.CompiledPlanner) -> None:
        """Set new planner for the motor."""
