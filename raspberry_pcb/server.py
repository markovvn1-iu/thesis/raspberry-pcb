import time
from concurrent.futures import Future
from typing import Optional
from weakref import ReferenceType

from loguru import logger
from networking.pollers import BasePollEvent, SimplePoller
from networking.pydantic import (
    NewPydanticClientMessageEvent,
    PydanticClientConnectedEvent,
    PydanticClientDisconnectedEvent,
    PydanticServer,
    PydanticServerClient,
)
from raspberry_pcb_sdk.messages import PCBRequest, PCBResponse
from raspberry_pcb_sdk.typing import PCBInfo, PCBState
from typing_extensions import TypeAlias

from raspberry_pcb.excpetions import BadRequestException, NotCalibratedException
from raspberry_pcb.pcb import RaspberryPCB

RaspberryServerClientPCB: TypeAlias = PydanticServerClient[PCBResponse, PCBRequest]


class RaspberryServerPCB:

    DISABLE_AFTER_NS = 60_000_000_000

    _disable_time_ns: int = -1
    _last_future: Optional[Future[bool]] = None
    _pcb: RaspberryPCB

    def __init__(self, pcb: RaspberryPCB, host: str, port: int) -> None:
        super().__init__()
        logger.info(f"Open PydanticServer on host={host}, port={port}")
        self._server = PydanticServer(host, port, PCBResponse, PCBRequest)
        self._pcb = pcb

    def _future_callback(
        self, msg: PCBRequest, is_complete: bool, client: ReferenceType[RaspberryServerClientPCB]
    ) -> None:
        if (client_ := client()) is not None:
            client_.send(PCBResponse.action_result(msg.id, is_complete))

    def _process_request(self, msg: PCBRequest, client: ReferenceType[RaspberryServerClientPCB]) -> PCBResponse:
        if isinstance(msg.msg, PCBRequest.GetInfo):
            info = self._pcb.info
            return PCBResponse.info(msg.id, PCBInfo(max_speed=info.max_speed, max_pos=info.max_pos))
        if isinstance(msg.msg, PCBRequest.GetCurState):
            state = self._pcb.cur_state
            return PCBResponse.cur_state(
                msg.id,
                PCBState(
                    is_enabled=self._pcb.is_enabled,
                    is_calibrated=self._pcb.is_calibrated,
                    pos=state.pos,
                    vel=state.vel,
                ),
            )
        if isinstance(msg.msg, PCBRequest.Action):
            command = msg.msg.command
            self._disable_time_ns = time.monotonic_ns() + self.DISABLE_AFTER_NS  # if there will be error
            self._pcb.enable(True)
            if isinstance(command, PCBRequest.Action.Calibrate):
                future = self._pcb.calibrate()
            elif isinstance(command, PCBRequest.Action.P2P):
                future = self._pcb.p2p(command.pos, command.end_vel)
            elif isinstance(command, PCBRequest.Action.ConstVel):
                future = self._pcb.const_vel(command.vel, command.duration)
            elif isinstance(command, PCBRequest.Action.StopMotors):
                future = self._pcb.stop_motors(command.sleep_for)
            else:
                raise BadRequestException(f"Unknown command {command}")
            self._disable_time_ns = -1
            self._last_future = future
            future.add_done_callback(lambda x: self._future_callback(msg, x.result(), client))
            return PCBResponse.action_accepted(msg.id)

        raise BadRequestException(f"Unknown requets {msg.msg}")

    def _process_event(self, event: BasePollEvent) -> None:
        event.process_default()

        if isinstance(event, PydanticClientConnectedEvent):
            if (client := event.client()) is not None:
                logger.debug(f"Client with addr={client.addr} connected")
        if isinstance(event, PydanticClientDisconnectedEvent):
            logger.debug("Client disconnected")
        if isinstance(event, NewPydanticClientMessageEvent):
            msg: PCBRequest = event.msg
            logger.debug(f"Client send command {msg}")
            try:
                res = self._process_request(msg, event.client)
            except NotCalibratedException:
                res = PCBResponse.exception(msg.id, "PCB is not callibrated")
            except Exception as e:  # pylint: disable=broad-except
                logger.exception(e)
                res = PCBResponse.exception(msg.id, "Unknown exception")
            try:
                if (client := event.client()) is not None:
                    client.send(res)
            except Exception as e:  # pylint: disable=broad-except
                logger.exception(e)

    def run_server_forever(self) -> None:
        logger.info("Starting Server thread")

        poller = SimplePoller()
        poller.add(self._server)
        self._server.open()

        try:
            while True:
                if (self._last_future is not None) and (self._last_future.done()):
                    self._last_future = None
                    self._disable_time_ns = time.monotonic_ns() + self.DISABLE_AFTER_NS
                if (self._disable_time_ns >= 0) and (self._disable_time_ns < time.monotonic_ns()):
                    self._pcb.enable(False)
                    self._disable_time_ns = -1

                if (event := poller.spin_until_event(0.1)) is None:
                    continue
                self._process_event(event)
        finally:
            self._server.close()
            poller.close()
