from dataclasses import dataclass
from typing import Optional, overload

from raspberry_pcb.motors_cpp import motors_cpp

from .gpio import RaspberryGPIO
from .updater import IUpdate


@dataclass
class Event:
    notify_time_ns: int


@dataclass
class TimeEvent(Event):
    timer_id: int
    event_time_ns: int


@dataclass
class GPIOSwitchEvent(Event):
    pin: int
    value: int


class EventHandler:
    _base: motors_cpp.EventHandler

    def __init__(self) -> None:
        self._base = motors_cpp.EventHandler()

    @overload
    def wait(self) -> Event:
        ...

    @overload
    def wait(self, timeout: float) -> Optional[Event]:
        ...

    def wait(self, timeout: Optional[float] = None) -> Optional[Event]:
        e = self._base.wait(timeout or 0)
        if e is None:
            return None
        if isinstance(e, motors_cpp.TimeEvent):
            return TimeEvent(e.notify_time_ns, e.timer_id, e.event_time_ns)
        if isinstance(e, motors_cpp.GPIOSwitchEvent):
            return GPIOSwitchEvent(e.notify_time_ns, e.pin, e.value)
        if isinstance(e, motors_cpp.PythonEvent):
            return e.obj
        raise NotImplementedError(f"Not implemented event type: {type(e)}")

    def push(self, event: Event) -> None:
        self._base.push(motors_cpp.PythonEvent(event.notify_time_ns, event))


class EventNotifier(IUpdate):
    pass


class TimeEventNotifier(EventNotifier):
    __base: motors_cpp.TimeEventNotifier
    _timer_id: int

    @property
    def _base(self) -> motors_cpp.TimeEventNotifier:
        return self.__base

    @property
    def timer_id(self) -> int:
        return self._timer_id

    def __init__(self, core: EventHandler, timer_id: int) -> None:
        super().__init__()
        self._timer_id = timer_id
        self.__base = motors_cpp.TimeEventNotifier(core._base, timer_id)

    def set_notify_time_ns(self, time_ns: int) -> None:
        self.__base.set_notify_time_ns(time_ns)


class GPIOSwitchEventNotifier(EventNotifier):
    __base: motors_cpp.GPIOSwitchEventNotifier

    @property
    def _base(self) -> motors_cpp.GPIOSwitchEventNotifier:
        return self.__base

    def __init__(self, core: EventHandler, gpio: RaspberryGPIO, pin: int, cooldown_time_ns: int = 0) -> None:
        super().__init__()
        self.__base = motors_cpp.GPIOSwitchEventNotifier(core._base, gpio._base, pin, cooldown_time_ns)
