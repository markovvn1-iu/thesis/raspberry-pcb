from typing import Optional

from raspberry_pcb.motors_cpp import motors_cpp

from .step_motor import ISoftwareStepMotorController


class RaspberryGPIO:
    INPUT = 0
    OUTPUT = 1

    __base: Optional[motors_cpp.RaspberryGPIO] = None
    _base: motors_cpp.RaspberryGPIO

    def __init__(self) -> None:
        if RaspberryGPIO.__base is None:
            RaspberryGPIO.__base = motors_cpp.RaspberryGPIO()
        self._base = RaspberryGPIO.__base

    def pinMode(self, pin: int, mode: int) -> None:
        self._base.pinMode(pin, mode)

    def digitalWrite(self, pin: int, value: int) -> None:
        self._base.digitalWrite(pin, value)

    def digitalRead(self, pin: int) -> int:
        return self._base.digitalRead(pin)


class SoftwareStepMotorGPIOController(ISoftwareStepMotorController):

    __base: motors_cpp.SoftwareStepMotorGPIOController

    @property
    def _base(self) -> motors_cpp.ISoftwareStepMotorController:
        return self.__base

    def __init__(self, gpio: RaspberryGPIO, dir_pin: int, step_pin: int, inverse: bool = False) -> None:
        super().__init__()
        self.__base = motors_cpp.SoftwareStepMotorGPIOController(gpio._base, dir_pin, step_pin, inverse)

    @property
    def real_pos(self) -> int:
        return self.__base.real_pos
