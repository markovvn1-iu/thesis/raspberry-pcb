#pragma once

#include <memory>
#include <atomic>
#include <stdexcept>

#include "updater.hpp"
#include "step_motor.hpp"


namespace motors {

struct PlanState {
    std::pair<int64_t, float> pos;
    double vel;

    PlanState(std::pair<int64_t, float> pos, double vel) : pos(pos), vel(vel) {}
};


class CompiledPlanner;


class Planner {
public:
    double duration;
    double delta_pos;
    double start_vel, end_vel;

    Planner(double delta_pos, double start_vel, double end_vel, double duration)
        : duration(duration), delta_pos(delta_pos), start_vel(start_vel), end_vel(end_vel) {
            if (duration > 100000) {
                throw std::runtime_error("duration is too long");
            }
        }

    virtual std::pair<double, double> get(double t) = 0;  // pos, vel
};

using PPlanner = std::shared_ptr<Planner>;


class CompiledPlanner {
private:
    PPlanner base_planner;

public:
    uint64_t start_time_ns, end_time_ns;
    std::pair<int64_t, float> start_pos, end_pos;
    double start_vel, end_vel;

    CompiledPlanner(PPlanner base_planner, uint64_t start_time_ns, std::pair<int64_t, float> start_pos);

    PlanState get(uint64_t cur_time_ns);
};

using PCompiledPlanner = std::shared_ptr<CompiledPlanner>;


class PlannerMotorController : public IUpdate {
private:
    PVelPosMotor motor;
    PCompiledPlanner planner;
    PlanState last_state = {{0, 0}, 0};

public:
    PlannerMotorController(PVelPosMotor motor) : motor(motor), IUpdate() {}

    void set_planner(PCompiledPlanner planner) {
        atomic_store(&this->planner, planner);
    }

    bool update(uint64_t cur_time_ns);
};

using PPlannerMotorController = std::shared_ptr<PlannerMotorController>;

} // namespace motors
