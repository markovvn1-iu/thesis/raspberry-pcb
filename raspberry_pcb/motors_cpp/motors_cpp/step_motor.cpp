#include "step_motor.hpp"
#include <pybind11/pybind11.h>

using namespace motors;
namespace py = pybind11;


double SoftwareStepMotor::calc_target_vel(const MotorState& cur_state, const MotorState& target_state) const {
    double dpos = (target_state.pos.first - cur_state.pos.first) + (double)(target_state.pos.second - cur_state.pos.second);
    double target_vel = target_state.vel + dpos * kp;
    return target_vel < -max_speed ? -max_speed : target_vel > max_speed ? max_speed : target_vel;
}

MotorState::Pos SoftwareStepMotor::update_cur_state(uint64_t cur_time_ns, const MotorStateWithTimestamp& cur_state, double target_vel) {
    const MotorState::Pos& last_pos = cur_state.state.pos;
    double pos_part = last_pos.second + target_vel / 1e9 * (cur_time_ns - cur_state.timestamp);
    int64_t pos_int_part = static_cast<int64_t>(floor(pos_part));

    MotorStateWithTimestamp new_state(cur_time_ns, {{last_pos.first + pos_int_part, (float)(pos_part - pos_int_part)}, target_vel});
    last_state.store(new_state);
    return new_state.state.pos;
}

bool SoftwareStepMotor::update(uint64_t cur_time_ns) {
    bool was_updated = controller->update(cur_time_ns);

    MotorState target_state = this->target_state.load();
    MotorStateWithTimestamp last_state = this->last_state.load();

    double target_vel = calc_target_vel(last_state.state, target_state);
    MotorState::Pos pos = update_cur_state(cur_time_ns, last_state, target_vel);

    if (pos.first == last_state.state.pos.first) return was_updated;

    // High-frequency noise protection
    int64_t old_target_pos = target_pos;
    if (target_vel > 0 && pos.first > target_pos) {
        target_pos = pos.first;
    } else if (target_vel < 0 && pos.first + 1 < target_pos) {
        target_pos = pos.first + 1;
    }
    if (target_pos == old_target_pos) return was_updated;

    controller->move(target_pos - old_target_pos);

    return was_updated || controller->update(cur_time_ns);
}
