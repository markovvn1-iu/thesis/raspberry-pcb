#pragma once

#include <time.h>
#include <cstdint>


class PreciseTimer {
private:
    uint32_t frequency;

    int tfd;
    bool allow_skips;
    struct itimerspec it_timer_spec = {{0, 0}, {0, 0}};

    timespec start_time;
    uint64_t iter_count = 0;
    uint64_t start_iter_time_ns = 0;

public:
    PreciseTimer(uint32_t frequency, bool allow_skips = false, bool busy_waiting = false);

    uint64_t get();

    uint64_t sleep();
};
