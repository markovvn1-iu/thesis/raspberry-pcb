#include "planner.hpp"

using namespace motors;


std::pair<int64_t, float> operator+(std::pair<int64_t, float> p, double delta) {
    double p_part = p.second + delta;
    int64_t p_int_part = static_cast<int64_t>(floor(p_part));
    return {p.first + p_int_part, (float)(p_part - p_int_part)};
}


CompiledPlanner::CompiledPlanner(PPlanner base_planner, uint64_t start_time_ns, std::pair<int64_t, float> start_pos)
    : base_planner(base_planner), start_time_ns(start_time_ns), start_pos(start_pos) {
        end_time_ns = start_time_ns + (uint64_t)(std::round(base_planner->duration * 1e9));
        start_vel = base_planner->start_vel;
        end_vel = base_planner->start_vel;
        end_pos = start_pos + base_planner->delta_pos;
    }


PlanState CompiledPlanner::get(uint64_t cur_time_ns) {
    if (cur_time_ns < start_time_ns) cur_time_ns = start_time_ns;
    std::pair<double, double> pos_vel = base_planner->get((cur_time_ns - start_time_ns) / 1e9);
    return PlanState(start_pos + pos_vel.first, pos_vel.second);
}

bool PlannerMotorController::update(uint64_t cur_time_ns) {
    PCompiledPlanner planner = atomic_load(&this->planner);
    if (planner) last_state = planner->get(cur_time_ns);
    motor->set_target_pos_vel(last_state.pos, last_state.vel);
    return motor->update(cur_time_ns);
    return false;
}
