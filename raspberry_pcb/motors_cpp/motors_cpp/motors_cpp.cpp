#include <pybind11/stl.h>
#include <pybind11/pybind11.h>

#include "gpio.hpp"
#include "step_motor.hpp"
#include "updater.hpp"
#include "planner.hpp"
#include "simple_planners.hpp"
#include "events.hpp"


namespace py = pybind11;
using namespace motors;

void init_updater(py::module &m) {
    py::class_<IUpdate, PIUpdate>(m, "IUpdate");

    py::class_<Updater>(m, "Updater")
        .def(py::init<>())
        .def("start", &Updater::start)
        .def("stop", &Updater::stop)
        .def("add", &Updater::add)
        .def("remove", &Updater::remove)
        .def_property_readonly("current_frequency", &Updater::get_current_frequency);
}

void init_gpio(py::module &m) {
    py::class_<RaspberryGPIO, PRaspberryGPIO>(m, "RaspberryGPIO")
        .def(py::init<>())
        .def("pinMode", &RaspberryGPIO::pinMode)
        .def("digitalWrite", &RaspberryGPIO::digitalWrite)
        .def("digitalRead", &RaspberryGPIO::digitalRead);

    py::class_<SoftwareStepMotorGPIOController, ISoftwareStepMotorController, std::shared_ptr<SoftwareStepMotorGPIOController>>(m, "SoftwareStepMotorGPIOController")
        .def(py::init<PRaspberryGPIO, int, int, bool>(), py::arg("gpio"), py::arg("dir_pin"), py::arg("step_pin"), py::arg("inverse"))
        .def_property_readonly("real_pos", &SoftwareStepMotorGPIOController::get_real_pos);
}


void init_step_motor(py::module &m) {
    py::class_<ISoftwareStepMotorController, PISoftwareStepMotorController>(m, "ISoftwareStepMotorController");
    
    py::class_<VelPosMotor, IUpdate, PVelPosMotor>(m, "VelPosMotor")
        .def_property_readonly("cur_state", &VelPosMotor::py_cur_state)
        .def("set_target_pos_vel", &VelPosMotor::set_target_pos_vel);

    py::class_<SoftwareStepMotor, VelPosMotor, PSoftwareStepMotor>(m, "SoftwareStepMotor")
        .def(py::init<PISoftwareStepMotorController, double, double>(), py::arg("controller"), py::arg("max_speed"), py::arg("kp"));
}

void init_planner(py::module &m) {

    py::class_<Planner, PPlanner>(m, "Planner")
        .def_readonly("duration", &Planner::duration)
        .def_readonly("delta_pos", &Planner::delta_pos)
        .def_readonly("start_vel", &Planner::start_vel)
        .def_readonly("end_vel", &Planner::end_vel)
        .def("get", &Planner::get);

    py::class_<PlanState>(m, "PlanState")
        .def_readonly("pos", &PlanState::pos)
        .def_readonly("vel", &PlanState::vel);

    py::class_<CompiledPlanner, PCompiledPlanner>(m, "CompiledPlanner")
        .def(py::init<PPlanner, uint64_t, std::pair<int64_t, float>>())
        .def_readonly("start_time_ns", &CompiledPlanner::start_time_ns)
        .def_readonly("end_time_ns", &CompiledPlanner::end_time_ns)
        .def_readonly("start_pos", &CompiledPlanner::start_pos)
        .def_readonly("end_pos", &CompiledPlanner::end_pos)
        .def_readonly("start_vel", &CompiledPlanner::start_vel)
        .def_readonly("end_vel", &CompiledPlanner::end_vel)
        .def("get", &CompiledPlanner::get);

    py::class_<PlannerMotorController, IUpdate, PPlannerMotorController>(m, "PlannerMotorController")
        .def(py::init<PVelPosMotor>())
        .def("set_planner", &PlannerMotorController::set_planner);
}

void init_events(py::module &m) {
    py::class_<Event, PEvent>(m, "Event")
        .def_readonly("notify_time_ns", &Event::notify_time_ns);

    py::class_<EventHandler, PEventHandler>(m, "EventHandler")
        .def(py::init<>())
        .def("wait", &EventHandler::wait, py::call_guard<py::gil_scoped_release>())
        .def("push", &EventHandler::push);

    py::class_<EventNotifier, IUpdate, PEventNotifier>(m, "EventNotifier");

    py::class_<PythonEvent, Event, PPythonEvent>(m, "PythonEvent")
        .def(py::init<uint64_t, py::object>())
        .def_readonly("obj", &PythonEvent::obj);

    py::class_<TimeEvent, Event, PTimeEvent>(m, "TimeEvent")
        .def_readonly("event_time_ns", &TimeEvent::event_time_ns)
        .def_readonly("timer_id", &TimeEvent::timer_id);

    py::class_<TimeEventNotifier, EventNotifier, PTimeEventNotifier>(m, "TimeEventNotifier")
        .def(py::init<PEventHandler, int>())
        .def("set_notify_time_ns", &TimeEventNotifier::set_notify_time_ns);

    py::class_<GPIOSwitchEvent, Event, PGPIOSwitchEvent>(m, "GPIOSwitchEvent")
        .def_readonly("pin", &GPIOSwitchEvent::pin)
        .def_readonly("value", &GPIOSwitchEvent::value);

    py::class_<GPIOSwitchEventNotifier, EventNotifier, PGPIOSwitchEventNotifier>(m, "GPIOSwitchEventNotifier")
        .def(py::init<PEventHandler, PRaspberryGPIO, int, uint64_t>());
}

void init_simple_planners(py::module &m) {

    py::class_<SimplePlanner, Planner, std::shared_ptr<SimplePlanner>>(m, "SimplePlanner")
        .def(py::init<double, double, double>());

    py::class_<SimpleConstPlanner, Planner, std::shared_ptr<SimpleConstPlanner>>(m, "SimpleConstPlanner")
        .def(py::init<double, double>());

    py::class_<GroupPlanner, Planner, std::shared_ptr<GroupPlanner>>(m, "GroupPlanner")
        .def(py::init<const std::vector<PPlanner>&>());
}

namespace mcl {

PYBIND11_MODULE(motors_cpp, m) {
    m.doc() = "Motors cpp library";

    init_updater(m);
    init_step_motor(m);
    init_gpio(m);
    init_planner(m);
    init_simple_planners(m);
    init_events(m);
}
}
