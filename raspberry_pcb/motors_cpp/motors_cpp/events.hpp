#pragma once

#include <memory>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <atomic>

#include <pybind11/pybind11.h>

#include "updater.hpp"
#include "gpio.hpp"


namespace py = pybind11;

namespace motors {

struct Event {
    uint64_t notify_time_ns;

    Event(uint64_t notify_time_ns) : notify_time_ns(notify_time_ns) {}
    virtual ~Event() {}
};

using PEvent = std::shared_ptr<Event>;


class EventHandler {
private:
    std::queue<PEvent> q;
    std::mutex m;
    std::condition_variable cv;

public:
    PEvent wait(float timeout = 0);
    void push(const PEvent& event);
};

using PEventHandler = std::shared_ptr<EventHandler>;


class EventNotifier : public IUpdate {
protected:
    PEventHandler core;

public:
    EventNotifier(PEventHandler core) : IUpdate(), core(core) {}
    virtual ~EventNotifier() {}
};

using PEventNotifier = std::shared_ptr<EventNotifier>;


struct PythonEvent : public Event {
    py::object obj;

    PythonEvent(uint64_t notify_time_ns, py::object obj) : Event(notify_time_ns), obj(obj) {}
    virtual ~PythonEvent() {}
};

using PPythonEvent = std::shared_ptr<PythonEvent>;


struct TimeEvent : public Event {
    int timer_id;
    uint64_t event_time_ns;

    TimeEvent(uint64_t notify_time_ns, int timer_id, uint64_t event_time_ns) : Event(notify_time_ns), timer_id(timer_id), event_time_ns(event_time_ns) {}
    virtual ~TimeEvent() {}
};

using PTimeEvent = std::shared_ptr<TimeEvent>;


class TimeEventNotifier : public EventNotifier {
private:
    int timer_id;
    uint64_t nofity_time_ns = 0;
    std::atomic<uint64_t> dirty_nofity_time_ns = 0;  // For thread-safe

public:
    TimeEventNotifier(PEventHandler core, int timer_id) : EventNotifier(core), timer_id(timer_id) {}
    virtual ~TimeEventNotifier() {}

    void set_notify_time_ns(uint64_t time_ns) {
        dirty_nofity_time_ns = time_ns;
    }

    bool update(uint64_t cur_time_ns);
};

using PTimeEventNotifier = std::shared_ptr<TimeEventNotifier>;



struct GPIOSwitchEvent : public Event {
    int pin;
    int value;

    GPIOSwitchEvent(uint64_t notify_time_ns, int pin, int value) : Event(notify_time_ns), pin(pin), value(value) {}
    virtual ~GPIOSwitchEvent() {}
};

using PGPIOSwitchEvent = std::shared_ptr<GPIOSwitchEvent>;


class GPIOSwitchEventNotifier : public EventNotifier {
private:
    PRaspberryGPIO gpio;
    int pin;
    uint64_t next_update_time_ns = 0;
    bool update_next_time = true;
    int old_value = -1;
    uint64_t cooldown_time_ns;

    bool cooling_down = false;
    uint64_t next_event_time_ns = 0;
    int last_event_value = -1;

    bool push_to_queue(uint64_t cur_time_ns, int value);

public:
    GPIOSwitchEventNotifier(PEventHandler core, PRaspberryGPIO gpio, int pin, uint64_t cooldown_time_ns = 0);
    virtual ~GPIOSwitchEventNotifier() {}

    bool update(uint64_t cur_time_ns);
};

using PGPIOSwitchEventNotifier = std::shared_ptr<GPIOSwitchEventNotifier>;


}  // namespace motors
