#pragma once

#include <cmath>
#include <atomic>

#include "updater.hpp"


namespace motors {

class ISoftwareStepMotorController {
public:
    virtual ~ISoftwareStepMotorController() {}
    virtual void move(int64_t step) = 0;
    virtual bool update(uint64_t cur_time_ns) = 0;
};

using PISoftwareStepMotorController = typename std::shared_ptr<ISoftwareStepMotorController>;


struct MotorState {
    using Pos = struct { int64_t first; float second; };

    Pos pos;
    double vel;

    MotorState(Pos pos, double vel) : pos(pos), vel(vel) {}
};


struct MotorStateWithTimestamp {
    uint64_t timestamp;
    MotorState state;

    MotorStateWithTimestamp(uint64_t timestamp, const MotorState& state) : timestamp(timestamp), state(state) {}
};


class VelPosMotor : public IUpdate {
protected:
    std::atomic<MotorState> target_state;
    std::atomic<MotorStateWithTimestamp> last_state;

public:
    VelPosMotor() : IUpdate(), last_state({0, {{0, 0}, 0}}), target_state({{0, 0}, 0}) {}
    virtual ~VelPosMotor() {}

    std::tuple<uint64_t, int64_t, float, double> py_cur_state() const {
        MotorStateWithTimestamp state = last_state.load();
        return {state.timestamp, state.state.pos.first, state.state.pos.second, state.state.vel};
    }

    void set_target_pos_vel(std::pair<int64_t, float> pos, double vel) {
        target_state.store(MotorState({pos.first, pos.second}, vel));
    }
};

using PVelPosMotor = typename std::shared_ptr<VelPosMotor>;


class SoftwareStepMotor : public VelPosMotor {
private:
    double max_speed;
    double kp;

    PISoftwareStepMotorController controller;

    int64_t target_pos = 0;  // Target position of the motor

    double calc_target_vel(const MotorState& cur_state, const MotorState& target_state) const;
    MotorState::Pos update_cur_state(uint64_t delta_time_ns, const MotorStateWithTimestamp& last_state, double target_vel);

public:
    SoftwareStepMotor(PISoftwareStepMotorController controller, double max_speed, double kp = 1)
        : VelPosMotor(), controller(controller), max_speed(max_speed), kp(kp) {}

    bool update(uint64_t cur_time_ns) override;
};

using PSoftwareStepMotor = typename std::shared_ptr<SoftwareStepMotor>;

} // namespace motors



