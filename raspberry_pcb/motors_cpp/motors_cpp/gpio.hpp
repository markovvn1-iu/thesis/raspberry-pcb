#pragma once

#include "step_motor.hpp"
#ifndef DISABLE_GPIO
    #include "wiringPi/wiringPi.h"
#else
    #define	INPUT			 0
    #define	OUTPUT			 1
#endif


namespace motors {

class RaspberryGPIO {
public:
    RaspberryGPIO();

    void pinMode(int pin, int mode) {
#ifndef DISABLE_GPIO
        ::pinMode(pin, mode);
#endif
    }

    void digitalWrite(int pin, int value) {
#ifndef DISABLE_GPIO        
        ::digitalWrite(pin, value);
#endif
    }

    int digitalRead(int pin) {
#ifndef DISABLE_GPIO
        return ::digitalRead(pin);
#else
        return 0;
#endif
    }
};

using PRaspberryGPIO = typename std::shared_ptr<RaspberryGPIO>;


class SoftwareStepMotorGPIOController : public ISoftwareStepMotorController {
    PRaspberryGPIO gpio;
    bool inverse;
    int dir_pin, step_pin;

    uint64_t next_update_ns = 0;
    bool last_step_edge = false;  // true - rasing edge, false - falling edge
    int last_direction = 0; // 1 - positive, -1 - negative

    int64_t real_pos = 0, target_pos = 0;

public:
    SoftwareStepMotorGPIOController(PRaspberryGPIO gpio, int dir_pin, int step_pin, bool inverse);
    virtual ~SoftwareStepMotorGPIOController();

    int64_t get_real_pos() { return real_pos; }

    virtual void move(int64_t step) override;
    virtual bool update(uint64_t cur_time_ns) override;
};

}  // namespace motors
