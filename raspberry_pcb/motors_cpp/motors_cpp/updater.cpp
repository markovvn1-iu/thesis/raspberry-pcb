#include "updater.hpp"
#include <stdexcept>
#include <algorithm>
#include <time.h>
#include <thread>

#include "precise_timer.hpp"

using namespace motors;



void _worker_thread(std::shared_ptr<MotorUpdaterThreadArgs>* raw_args) {
    std::shared_ptr<MotorUpdaterThreadArgs> args(*((std::shared_ptr<MotorUpdaterThreadArgs>*)raw_args));
    delete raw_args;

    PreciseTimer timer(args->target_update_frequency, args->allow_skips, args->busy_waiting);

    uint64_t start_time = timer.get();

    while (args->is_working) {
        std::shared_ptr<std::vector<PIUpdate>> pobjs = args->objs;
        const std::vector<PIUpdate>& objs = *pobjs;

        uint64_t cur_time_ns = start_time;

        for (int i = 0; i < objs.size(); i++) {
            if (objs[i]->update(cur_time_ns)) {
                cur_time_ns = timer.get();
            }
        }

        timer.sleep();

        uint64_t old_time = start_time;
        start_time = timer.get();
        float update_latency_ns = (float)(start_time - old_time);
        args->update_latency_ns = args->update_latency_ns * UPDATE_LATENCY_K + (1 - UPDATE_LATENCY_K) * update_latency_ns;
    }
}

void Updater::set_objs(const std::vector<PIUpdate>& objs) {
    if (thread_data == NULL) return;
    thread_data->objs.reset(new std::vector<PIUpdate>(objs));
}

void Updater::start(uint32_t update_frequency, bool allow_skips, bool busy_waiting) {
    if (thread_data != NULL) {
        stop();
        thr.join();
    };

    thread_data.reset(new MotorUpdaterThreadArgs);
    thread_data->allow_skips = allow_skips;
    thread_data->busy_waiting = busy_waiting;
    thread_data->target_update_frequency = update_frequency;
    thread_data->objs.reset(new std::vector<PIUpdate>(objs));

    thr = std::thread(_worker_thread, new std::shared_ptr<MotorUpdaterThreadArgs>(thread_data));
    thr.detach();  // autorelease memory (no need to call join)
}

void Updater::stop() {
    if (thread_data == NULL) return;
    thread_data->is_working = false;
    thread_data = NULL;
}

void Updater::add(PIUpdate obj) {
    objs.push_back(obj);
    set_objs(objs);
}

bool Updater::remove(PIUpdate obj) {
    auto it = std::find(objs.begin(), objs.end(), obj);
    if (it == objs.end()) return false;
    objs.erase(it);
    set_objs(objs);
    return true;
}
