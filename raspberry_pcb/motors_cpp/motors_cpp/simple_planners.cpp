#include "simple_planners.hpp"

#include <stdexcept>

using namespace motors;


std::pair<double, double> SimplePlanner::get(double t) {
    if (t <= 0) return {0, start_vel};

    double vel;
    if (t < duration) {
        vel = (end_vel - start_vel) * (t / duration) + start_vel;
    } else {
        vel = end_vel;
        t = duration;
    }

    double pos = (start_vel + vel) / 2 * t;
    return {pos, vel};
}


std::pair<double, double> SimpleConstPlanner::get(double t) {
    if (t <= 0) return {0, start_vel};
    if (t > duration) t = duration;
    return {start_vel * t, start_vel};
}

GroupPlanner::GroupPlanner(const std::vector<PPlanner>& planners) : Planner(0, 0, 0, 0), planners(planners) {
    if (planners.size() <= 0)
        throw std::runtime_error("GroupPlanner must have at least one Planner");

    start_vel = (*planners.begin())->start_vel;
    end_vel = (*planners.rbegin())->end_vel;

    start_time.reserve(planners.size() + 1);
    start_pos.reserve(planners.size() + 1);

    start_time.push_back(0);
    start_pos.push_back(0);

    for (const PPlanner& planner : planners) {
        duration += planner->duration;
        delta_pos += planner->delta_pos;

        start_time.push_back(duration);
        start_pos.push_back(delta_pos);
    }
}

std::pair<double, double> GroupPlanner::get(double t) {
    while (last_planner_i > 0 && start_time[last_planner_i] > t) {
        last_planner_i--;
    }
    while (last_planner_i < planners.size() - 1 && start_time[last_planner_i + 1] <= t) {
        last_planner_i++;
    }
    std::pair<double, double> pos_vel = planners[last_planner_i]->get(t - start_time[last_planner_i]);
    pos_vel.first += start_pos[last_planner_i];
    return pos_vel;
}
