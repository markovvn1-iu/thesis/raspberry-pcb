#pragma once

#include <cmath>
#include <vector>
#include <memory>
#include <thread>
#include <atomic>

#define UPDATE_LATENCY_K 0.95


namespace motors {

class IUpdate {
public:
    virtual ~IUpdate() {}
    virtual bool update(uint64_t cur_time_ns) = 0;
};

using PIUpdate = typename std::shared_ptr<IUpdate>; 


struct MotorUpdaterThreadArgs {
    bool allow_skips;
    bool busy_waiting;
    bool is_working = true;
    uint32_t target_update_frequency;  // updates per second
    std::shared_ptr<std::vector<PIUpdate>> objs;
    
    std::atomic<float> update_latency_ns;
};


class Updater {
private:
    std::thread thr;
    std::vector<PIUpdate> objs;
    std::shared_ptr<MotorUpdaterThreadArgs> thread_data = NULL;

    void set_objs(const std::vector<PIUpdate>& objs);

public:
    Updater() {}

    void start(uint32_t update_frequency, bool allow_skips, bool busy_waiting);
    void stop();

    void add(PIUpdate obj);
    bool remove(PIUpdate obj);

    float get_current_frequency() {
        if (!thread_data) return 0;
        float update_latency_ns = thread_data->update_latency_ns;
        return (update_latency_ns == 0) ? 0 : 1e9 / update_latency_ns;
    }
};

}



