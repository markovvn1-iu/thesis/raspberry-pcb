#pragma once

#include "planner.hpp"

namespace motors {

class SimplePlanner : public Planner {
public:
    SimplePlanner(double start_vel, double end_vel, double duration)
        : Planner((start_vel + end_vel) / 2 * duration, start_vel, end_vel, duration) {}

    std::pair<double, double> get(double t) override;
};


class SimpleConstPlanner : public Planner {
public:
    SimpleConstPlanner(double vel, double duration)
        : Planner(vel * duration, vel, vel, duration) {}

    std::pair<double, double> get(double t) override;
};


class GroupPlanner : public Planner {
private:
    std::vector<PPlanner> planners;
    int last_planner_i = 0;
    std::vector<double> start_time;
    std::vector<double> start_pos;

public:
    GroupPlanner(const std::vector<PPlanner>& planners);

    std::pair<double, double> get(double t) override;
};

} // namespace motors
