#include "gpio.hpp"

#define GPIO_DELAY_NS 1000

using namespace motors;

RaspberryGPIO::RaspberryGPIO() {
#ifndef DISABLE_GPIO
    wiringPiSetupGpio();
#endif
}

SoftwareStepMotorGPIOController::SoftwareStepMotorGPIOController(PRaspberryGPIO gpio, int dir_pin, int step_pin, bool inverse)
    : ISoftwareStepMotorController(), gpio(gpio), dir_pin(dir_pin), step_pin(step_pin), inverse(inverse) {
    gpio->pinMode(dir_pin, OUTPUT); // dir
    gpio->pinMode(step_pin, OUTPUT); // step
}

SoftwareStepMotorGPIOController::~SoftwareStepMotorGPIOController() {

}

void SoftwareStepMotorGPIOController::move(int64_t step) {
    target_pos += step;
}

bool SoftwareStepMotorGPIOController::update(uint64_t cur_time_ns) {
    if (!last_step_edge && real_pos == target_pos) return false;
    if (cur_time_ns < next_update_ns) return false;
    next_update_ns = cur_time_ns + GPIO_DELAY_NS;

    if (last_step_edge) {
        last_step_edge = false;
        gpio->digitalWrite(step_pin, 0);
        return true;
    }

    int new_direction = target_pos > real_pos ? 1 : -1;
    if (last_direction != new_direction) {
        last_direction = new_direction;
        gpio->digitalWrite(dir_pin, (new_direction > 0) != inverse);
        return true;
    }

    last_step_edge = true;
    gpio->digitalWrite(step_pin, 1);
    real_pos += new_direction;
    return true;
}
