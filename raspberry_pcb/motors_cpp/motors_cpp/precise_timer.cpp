#include "precise_timer.hpp"
#include <stdexcept>
#include <sys/timerfd.h>
#include <unistd.h>


PreciseTimer::PreciseTimer(uint32_t frequency, bool allow_skips, bool busy_waiting) : frequency(frequency), allow_skips(allow_skips) {
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    if (busy_waiting) {
        tfd = 0;
    } else {
        tfd = timerfd_create(CLOCK_MONOTONIC,  0);
        if (tfd <= 0) throw std::runtime_error("timerfd_create() failed");
    }
}

uint64_t PreciseTimer::get() {
    timespec cur_time;
    clock_gettime(CLOCK_MONOTONIC, &cur_time);
    return (cur_time.tv_sec - start_time.tv_sec) * (uint64_t)1000000000 + (cur_time.tv_nsec - start_time.tv_nsec);
}

uint64_t PreciseTimer::sleep() {
    if (frequency == 0) return false;

    iter_count += 1;
    uint64_t next_iter_time_ns = start_iter_time_ns + 1000000000 * iter_count / frequency;
    uint64_t cur_time_ns = get();
    if (next_iter_time_ns <= cur_time_ns) {  // if too slow
        if (allow_skips) {
            iter_count = 0;
            start_iter_time_ns = cur_time_ns;
        }
        return iter_count;
    }

    if (iter_count > 1000000000) { // just to be on the safe side
        iter_count = 0;
        start_iter_time_ns = next_iter_time_ns;
    }

    if (tfd == 0) {
        while (get() < next_iter_time_ns);
    } else {
        it_timer_spec.it_value.tv_nsec = static_cast<int64_t>(next_iter_time_ns - cur_time_ns);
        if (timerfd_settime(tfd, 0, &it_timer_spec, NULL) != 0)
            throw std::runtime_error("timerfd_settime() failed");

        uint64_t dummybuf;
        int expired = read(tfd, &dummybuf, sizeof(uint64_t));
        if (expired < 0) throw std::runtime_error("read() for timer failed");
    }

    return iter_count;
}
