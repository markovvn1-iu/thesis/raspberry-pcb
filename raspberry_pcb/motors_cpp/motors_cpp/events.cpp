#include "events.hpp"

#include <algorithm>

using namespace motors;


void EventHandler::push(const PEvent& event) {
    std::lock_guard<std::mutex> lock(m);
    q.push(event);
    cv.notify_one();
}

PEvent EventHandler::wait(float timeout) {
    std::unique_lock<std::mutex> lock(m);
    if (timeout <= 0) {
        cv.wait(lock, [&]{return !q.empty();});    
    } else {
        if (!cv.wait_for(lock, std::chrono::duration<float>(timeout), [&]{return !q.empty();})) return PEvent();
    }
    PEvent res = q.front();
    q.pop();
    return res;
}

bool TimeEventNotifier::update(uint64_t cur_time_ns) {
    uint64_t dirty_nofity_time_ns = this->dirty_nofity_time_ns.exchange(0);
    if (dirty_nofity_time_ns != 0) nofity_time_ns = dirty_nofity_time_ns;
    
    if (nofity_time_ns == 0 || cur_time_ns < nofity_time_ns) return false;
    core->push(std::make_shared<TimeEvent>(cur_time_ns, timer_id, nofity_time_ns));
    nofity_time_ns = 0;
    return false;
}

GPIOSwitchEventNotifier::GPIOSwitchEventNotifier(PEventHandler core, PRaspberryGPIO gpio, int pin, uint64_t cooldown_time_ns)
    : EventNotifier(core), gpio(gpio), pin(pin), cooldown_time_ns(cooldown_time_ns) {
    gpio->pinMode(pin, INPUT);
}

bool GPIOSwitchEventNotifier::push_to_queue(uint64_t cur_time_ns, int value) {
    cooling_down = false;
    if (last_event_value == value) return false;
    last_event_value = value;
    core->push(std::make_shared<GPIOSwitchEvent>(cur_time_ns, pin, value));
    next_event_time_ns = cur_time_ns + cooldown_time_ns;
    return true;
}

bool GPIOSwitchEventNotifier::update(uint64_t cur_time_ns) {
    if (cooling_down && cur_time_ns >= next_event_time_ns) {
        push_to_queue(cur_time_ns, old_value);
        return false;
    }

    if (update_next_time) {
        update_next_time = false;
        next_update_time_ns += (rand() % 50000) + 50000;
        if (next_update_time_ns < cur_time_ns) next_update_time_ns = cur_time_ns;
        return true;
    }
    if (cur_time_ns < next_update_time_ns) return false;

    int value = gpio->digitalRead(pin);
    update_next_time = true;

    if (old_value == -1) old_value = value;
    if (old_value == value) return true;
    old_value = value;

    if (cur_time_ns < next_event_time_ns) {
        cooling_down = true;
        next_event_time_ns = cur_time_ns + cooldown_time_ns;
        return true;
    }

    push_to_queue(cur_time_ns, value);
    return true;
}
