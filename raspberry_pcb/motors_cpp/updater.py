from abc import ABC, abstractmethod
from typing import Set

from . import motors_cpp


class IUpdate(ABC):
    @property
    @abstractmethod
    def _base(self) -> motors_cpp.IUpdate:
        pass


class Updater:
    _motors: Set[IUpdate]
    _base: motors_cpp.Updater

    @property
    def current_frequency(self) -> float:
        return self._base.current_frequency

    def __init__(self) -> None:
        self._motors = set()
        self._base = motors_cpp.Updater()

    def __del__(self) -> None:
        for motor in self._motors:
            self._base.remove(motor._base)
        self._motors.clear()
        self._base.stop()

    def add(self, motor: IUpdate) -> None:
        self._motors.add(motor)
        self._base.add(motor._base)

    def remove(self, motor: IUpdate) -> None:
        self._motors.remove(motor)
        self._base.remove(motor._base)

    def start(self, frequency: int, allow_skips: bool = True, busy_waiting: bool = True) -> None:
        self._base.start(frequency, allow_skips, busy_waiting)

    def stop(self) -> None:
        self._base.stop()
