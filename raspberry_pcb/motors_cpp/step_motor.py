from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Tuple

from raspberry_pcb.motors_cpp import motors_cpp

from .updater import IUpdate


class ISoftwareStepMotorController(ABC):
    @property
    @abstractmethod
    def _base(self) -> motors_cpp.ISoftwareStepMotorController:
        pass


@dataclass
class MotorState:
    timestamp_ns: int
    pos: Tuple[int, float]
    vel: float


class VelPosMotor(IUpdate):
    @property
    @abstractmethod
    def _base(self) -> motors_cpp.VelPosMotor:
        pass

    @property
    def cur_state(self) -> MotorState:
        res: Tuple[int, int, float, float] = self._base.cur_state
        return MotorState(res[0], (res[1], res[2]), res[3])

    def set_target_pos_vel(self, pos: Tuple[int, float], vel: float) -> None:
        self._base.set_target_pos_vel(pos, vel)


class SoftwareStepMotor(VelPosMotor):

    __base: motors_cpp.SoftwareStepMotor

    @property
    def _base(self) -> motors_cpp.VelPosMotor:
        return self.__base

    def __init__(self, controller: ISoftwareStepMotorController, max_speed: float, kp: float) -> None:
        self.__base = motors_cpp.SoftwareStepMotor(controller._base, max_speed, kp)
        super().__init__()
