from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import List, Tuple

from raspberry_pcb.motors_cpp import motors_cpp

from .step_motor import VelPosMotor
from .updater import IUpdate


@dataclass(frozen=True)
class PlanState:
    pos: Tuple[int, float]
    vel: float


@dataclass(frozen=True)
class CompiledPlannerInfo:
    start_time_ns: int
    end_time_ns: int
    start_pos: Tuple[int, float]
    end_pos: Tuple[int, float]
    start_vel: float
    end_vel: float


class CompiledPlanner:
    _info: CompiledPlannerInfo
    _base: motors_cpp.CompiledPlanner

    @property
    def info(self) -> CompiledPlannerInfo:
        """Return information about the compiled planner."""
        return self._info

    def __init__(self, planner: motors_cpp.Planner, start_time_ns: int, start_pos: Tuple[int, float]) -> None:
        self._base = motors_cpp.CompiledPlanner(planner, start_time_ns, start_pos)
        self._info = CompiledPlannerInfo(
            self._base.start_time_ns,
            self._base.end_time_ns,
            self._base.start_pos,
            self._base.end_pos,
            self._base.start_vel,
            self._base.end_vel,
        )

    def __str__(self) -> str:
        return f"{self.__class__.__name__}(info={self._info})"

    def get(self, cur_time_ns: int) -> PlanState:
        res = self._base.get(cur_time_ns)
        return PlanState(res.pos, res.vel)


@dataclass(frozen=True)
class PlannerInfo:
    duration: float
    delta_pos: float
    start_vel: float
    end_vel: float


class Planner(ABC):
    _info: PlannerInfo

    @property
    @abstractmethod
    def _base(self) -> motors_cpp.Planner:
        pass

    @property
    def info(self) -> PlannerInfo:
        """Return information about the planner."""
        return self._info

    def __init__(self) -> None:
        super().__init__()
        self._info = PlannerInfo(
            self._base.duration,
            self._base.delta_pos,
            self._base.start_vel,
            self._base.end_vel,
        )

    def __str__(self) -> str:
        return f"{self.__class__.__name__}(info={self._info})"

    def compile(self, start_time_ns: int, start_pos: Tuple[int, float]) -> CompiledPlanner:
        """Compile the planner, prepare for execution."""
        return CompiledPlanner(self._base, start_time_ns, start_pos)

    def get(self, t: float) -> Tuple[float, float]:
        """Return position and velocity in given time."""
        return self._base.get(t)


class PlannerMotorController(IUpdate):
    __base: motors_cpp.PlannerMotorController

    @property
    def _base(self) -> motors_cpp.IUpdate:
        return self.__base

    def __init__(self, motor: VelPosMotor) -> None:
        self.__base = motors_cpp.PlannerMotorController(motor._base)
        super().__init__()

    def set_planner(self, planner: CompiledPlanner) -> None:
        self.__base.set_planner(planner._base)


class SimplePlanner(Planner):
    __base: motors_cpp.SimplePlanner

    @property
    def _base(self) -> motors_cpp.SimplePlanner:
        return self.__base

    def __init__(self, start_vel: float, end_vel: float, duration: float) -> None:
        if duration <= 0:
            raise ValueError("duration cannot be less than or equeal to 0")

        self.__base = motors_cpp.SimplePlanner(start_vel, end_vel, duration)
        super().__init__()


class SimpleConstPlanner(Planner):
    __base: motors_cpp.SimpleConstPlanner

    @property
    def _base(self) -> motors_cpp.SimpleConstPlanner:
        return self.__base

    def __init__(self, vel: float, duration: float) -> None:
        if duration < 0:
            raise ValueError("duration cannot be less than 0")

        self.__base = motors_cpp.SimpleConstPlanner(vel, duration)
        super().__init__()


class GroupPlanner(Planner):
    __base: motors_cpp.GroupPlanner

    @property
    def _base(self) -> motors_cpp.GroupPlanner:
        return self.__base

    def __init__(self, planners: List[Planner]) -> None:
        if len(planners) <= 0:
            raise ValueError("GroupPlanner must have at least one Planner")

        for p1, p2 in zip(planners[:-1], planners[1:]):
            if abs(p1.info.end_vel - p2.info.start_vel) > 1e-5:
                raise ValueError("planners in GroupPlanner are not smooth")

        self.__base = motors_cpp.GroupPlanner([p._base for p in planners])
        super().__init__()
