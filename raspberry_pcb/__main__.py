import argparse
import sys

from loguru import logger

from raspberry_pcb.pcb import RaspberryPCB
from raspberry_pcb.project import PROJECT_PATH
from raspberry_pcb.server import RaspberryServerPCB


def parse_args() -> argparse.Namespace:
    """Parse command line arguments.

    Examples of usage:
        python -m raspberry_pcb --host 0.0.0.0 --port 32585

    Returns:
        argparse.Namespace: arguments
    """

    parser = argparse.ArgumentParser(description="Server for remote PCB control")
    parser.add_argument("--host", type=str, default="0.0.0.0", help="Server host")
    parser.add_argument("--port", type=int, default=32585, help="Server port")
    return parser.parse_args()


logger.remove()
logger.add(sys.stderr, backtrace=True, diagnose=False)
logger.add(
    PROJECT_PATH / "logs/logs.log",
    rotation="10 MB",
    compression="zip",
    backtrace=True,
    diagnose=False,
)

args = parse_args()
logger.info("Starting the PCBRemoteServer")
pcb = RaspberryPCB()
pcb.start()

server = RaspberryServerPCB(pcb, args.host, args.port)
try:
    server.run_server_forever()
finally:
    pcb.enable(False)
    pcb.stop()
    logger.info("PCBRemoteServer is stoped")
