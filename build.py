import os
import platform
from setuptools_cpp import CMakeExtension, ExtensionBuilder


def build(setup_kwargs):
    if (platform.machine() != "aarch64"):
        os.environ["DISABLE_GPIO"] = "1"

    ext_modules = [
        CMakeExtension("raspberry_pcb.motors_cpp.motors_cpp", sourcedir="raspberry_pcb/motors_cpp/motors_cpp"),
    ]
    setup_kwargs.update({
        "ext_modules": ext_modules,
        "cmdclass": {"build_ext": ExtensionBuilder},
        "zip_safe": False,
    })
