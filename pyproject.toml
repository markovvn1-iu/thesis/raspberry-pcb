[tool.poetry]
name = "raspberry-pcb"
version = "0.3.0"
description = "Python server for remote PCB control"
authors = ["Vladimir Markov <markovvn1@gmail.com>"]
packages = [{include = "raspberry_pcb"}]

[tool.poetry.dependencies]
python = "^3.9"
loguru = "^0.6.0"
networking = {version = "^0.2.0", extras = ["pydantic"], source = "mirai-libs"}
pydantic = "^1.10.2"
pyproject-flake8 = "^6.0.0"
mypy = "^0.991"
setuptools-cpp = "^0.1.0"
raspberry-pcb-sdk = {version = "^0.1.1", source = "project"}

[tool.poetry.group.dev.dependencies]
isort = "^5.10.1"
black = "^22.3.0"
pylint = "^2.14.0"
pyproject-autoflake = "^1.0.1"
pytest = "^7.1.3"
pybind11 = "^2.10.0"
pytest-cov = "^4.0.0"
tqdm = "^4.64.1"
matplotlib = "^3.6.1"
pyqt5 = "^5.15.7"

[build-system]
requires = ["poetry-core>=1.0.0", "pybind11", "setuptools_cpp"]
build-backend = "poetry.core.masonry.api"

[tool.poetry.build]
script = "build.py"

[tool.flake8]
enable-extensions = "G"
exclude = [".git", ".venv"]
ignore = [
    "E203", # whitespace before ‘:’
    "E501", # line too long
    "W503", # line break before binary operator
    "W504", # Line break occurred after a binary operator
]
show-source = true
max-complexity = 15

[tool.pylint]
generated-members = "cv2.*"
max-module-lines = 300
output-format = "colorized"
max-line-length = 120
disable = [
    "C0111", # Missing module docstring (missing-docstring)
    "R0901", # Too many ancestors (m/n) (too-many-ancestors)
    "R0903", # Too few public methods (m/n) (too-few-public-methods)
    "R0914", # Too many local variables (m/n) (too-many-locals)
    "R0801", # Similar lines in files (duplicate-code)
    "C0103", # Class constant doesn't conform to UPPER_CASE naming style (invalid-name)
    "W0223", # Method is abstract in class but is not overridden (abstract-method)
    "W0212", # Access to a protected member of a client class (protected-access)
]
extension-pkg-whitelist = ["pydantic", "raspberry_pcb.motors_cpp.motors_cpp"]
# Maximum number of nested code blocks for function / method
max-nested-blocks=3
# Maximum number of branch for function / method body
max-branches=15
# Maximum number of statements in function / method body
max-statements=30
# Maximum number of attributes for a class (see R0902).
max-attributes=15

[tool.black]
line_length = 120

[tool.isort]
default_section = "THIRDPARTY"
profile = "black"
line_length = 120

[tool.mypy]
check_untyped_defs = true
disallow_any_generics = true
disallow_incomplete_defs = true
disallow_untyped_defs = true
ignore_missing_imports = true
no_implicit_optional = true

[mypy-raspberry_pcb.motors_cpp]
ignore_errors = true

[tool.pytest.ini_options]
addopts = "--cov=raspberry_pcb"

[tool.coverage.run]
branch = true
source = ["raspberry_pcb"]

[tool.coverage.report]
show_missing = true
skip_covered = true
exclude_lines = ["pragma: no cover", "def __repr__", "def __str__", "@overload"]

[[tool.poetry.source]]
name = "mirai-libs"
url = "https://gitlab.com/api/v4/groups/54262453/-/packages/pypi/simple"

[[tool.poetry.source]]
name = "project"
url = "https://gitlab.com/api/v4/groups/57681843/-/packages/pypi/simple"
