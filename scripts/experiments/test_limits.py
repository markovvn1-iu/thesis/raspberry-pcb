from typing import Optional, Tuple

import matplotlib.pyplot as plt

from raspberry_pcb.planners import GroupPlanner, P2PPlanner, Planner, SimpleConstPlanner, SimplePlanner

a_max = 100
v_max = 10
S = v_max * v_max / 2 / a_max


class LimitPlanner:
    _count = 0
    _planner: GroupPlanner
    _stop_time: Optional[float] = None
    _start_time_vel_pos: Optional[Tuple[float, float, float]] = None
    _start_time_pos: Optional[Tuple[float, float]] = None

    def __init__(self) -> None:
        self._planner = GroupPlanner(
            [
                SimplePlanner(0, -v_max, v_max / a_max),
                SimpleConstPlanner(-v_max, v_max / a_max),
                SimplePlanner(-v_max, 0, v_max / a_max),
                SimplePlanner(0, v_max, v_max / a_max),
                SimpleConstPlanner(v_max, v_max / a_max),
                SimplePlanner(v_max, 0, v_max / a_max),
            ]
        )

    def get(self) -> Tuple[float, float, float]:
        t = self._count / 1000 * self._planner.info.duration
        self._count += 1

        pos, vel = self._planner.get(t)
        pos += 1.5
        return t, pos, vel

    def update1(self) -> Tuple[float, float, float]:
        t, pos, vel = self.get()

        if pos < -S:
            return t, 0, 0
        if pos > vel * vel / 2 / a_max:
            return t, pos, vel

        if vel < 0:
            self._start_time_pos = None
            if self._stop_time is None:
                self._stop_time = t - vel / a_max
            dt = self._stop_time - t
            if dt < 0:
                return t, 0, 0
            return t, a_max * dt * dt / 2, -a_max * dt
        if (self._stop_time is not None) or (self._start_time_pos is not None):
            if self._start_time_pos is None:
                self._start_time_pos = (t, 0)
            if pos < self._start_time_pos[1]:
                dt = t - self._start_time_pos[0]
                dt2 = (S - pos) / vel
                vel_p = (S - self._start_time_pos[1]) / dt2
                # p = max(0, 0.5 * (S - self._start_time_pos[1]) / S) ** 2
                # print(p)
                # vel_v = vel
                vel = max(-v_max, min(v_max, vel_p * 2 - vel))  # + vel_v * p
                pos = self._start_time_pos[1] + vel * dt
                self._start_time_pos = (t, pos)
                return t, pos, vel

        self._stop_time = None
        return t, pos, vel

    def update2(self) -> Tuple[float, float, float]:
        t, pos, vel = self.get()

        if pos < -S:
            return t, 0, 0
        if pos > vel * vel / 2 / a_max:
            return t, pos, vel

        if vel < 0:
            self._start_time_vel_pos = None
            if self._stop_time is None:
                self._stop_time = t - vel / a_max
            dt = self._stop_time - t
            if dt < 0:
                return t, 0, 0
            return t, a_max * dt * dt / 2, -a_max * dt
        if (self._stop_time is not None) or (self._start_time_vel_pos is not None):
            if self._start_time_vel_pos is None:
                self._start_time_vel_pos = (t, 0, 0)
            if pos < self._start_time_vel_pos[2]:
                dt = t - self._start_time_vel_pos[0]

                dpos_max = (v_max * v_max - vel * vel) / 2 / a_max
                if dpos_max >= S - pos:
                    dt2 = ((vel * vel + 2 * a_max * (S - pos)) ** 0.5 - vel) / a_max
                else:
                    dt2 = (v_max - vel) / a_max + (S - pos - dpos_max) / v_max
                planner_ = P2PPlanner(self._start_time_vel_pos[1], S - self._start_time_vel_pos[2], vel, v_max, a_max)

                # print(planner_.info.duration, dt2)
                print(self._start_time_vel_pos[1], S - self._start_time_vel_pos[2], vel, v_max, a_max)
                planner__: Planner = planner_
                if planner_.info.duration < dt2:
                    planner__ = planner_.add_time(dt2 - planner_.info.duration)
                pos1, vel1 = planner__.get(dt)

                pos = self._start_time_vel_pos[2] + pos1
                vel = vel1
                self._start_time_vel_pos = (t, vel, pos)
                return t, pos, vel

        self._stop_time = None
        return t, pos, vel


planner = LimitPlanner()
data1 = [planner.get() for i in range(1001)]
planner = LimitPlanner()
data2 = [planner.update1() for i in range(1001)]

plt.plot([i[0] for i in data1], [i[1] for i in data1], "--", color="k")
plt.plot([i[0] for i in data1], [i[2] / 10 for i in data1], "--", color="g")
plt.plot([i[0] for i in data2], [i[1] for i in data2], color="k")
plt.plot([i[0] for i in data2], [i[2] / 10 for i in data2], color="g")
plt.plot([i[0] for i in data2[1:]], [(i1[2] - i2[2]) * 10 for i1, i2 in zip(data2, data2[1:])])
# plt.axis("equal")
plt.show()
