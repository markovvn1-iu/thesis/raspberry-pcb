from loguru import logger

from raspberry_pcb.motors_cpp import motors_cpp


@logger.catch
def main() -> None:
    gpio = motors_cpp.RaspberryGPIO()

    events = motors_cpp.EventHandler()
    switch_event = motors_cpp.GPIOSwitchEventNotifier(events, gpio, 15)

    updater = motors_cpp.Updater()
    updater.add(switch_event)
    updater.start(100000, True, True)

    for _ in range(10):
        e = events.wait()
        print(e, e.notify_time_ns)


if __name__ == "__main__":
    main()
