from dataclasses import dataclass

from loguru import logger

from raspberry_pcb.motors_cpp import cpp_events, cpp_updater


@dataclass
class MyEvent(cpp_events.Event):
    vvv: str
    tt: int


@logger.catch
def main() -> None:
    event_handler = cpp_events.EventHandler()
    time_event = cpp_events.TimeEventNotifier(event_handler, 0)

    updater = cpp_updater.Updater()
    updater.add(time_event)
    updater.start(0)

    # for i in range(10):
    print("start!")

    time_event.set_notify_time_ns(round(2 * 1e9))
    e = event_handler.wait()

    event_handler.push(MyEvent(8172638, "some string", 1345))
    e = event_handler.wait()

    print(e)


if __name__ == "__main__":
    main()
