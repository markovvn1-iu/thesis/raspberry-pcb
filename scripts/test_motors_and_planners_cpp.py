import matplotlib.pyplot as plt
from loguru import logger

from raspberry_pcb.motors_cpp import motors_cpp


@logger.catch
def main() -> None:
    gpio = motors_cpp.RaspberryGPIO()
    motor1_controller = motors_cpp.SoftwareStepMotorGPIOController(gpio, 3, 27, False)
    motor1 = motors_cpp.SoftwareStepMotor(motor1_controller, 21696, 0)
    motor1_planner = motors_cpp.PlannerMotorController(motor1)

    events = motors_cpp.EventHandler()
    time_event = motors_cpp.TimeEventNotifier(events, 0)

    updater = motors_cpp.Updater()
    updater.add(motor1_planner)
    updater.add(time_event)
    updater.start(100000, True, True)

    tt, p1, p2, vel = motor1.cur_state  # pylint: disable=unpacking-non-sequence
    print(tt, (p1, p2), vel)

    planner = motors_cpp.CompiledPlanner(motors_cpp.SimplePlanner(vel, 21696, int(1e9)), tt, (p1, p2))
    time_event.set_notify_time_ns(planner.end_time_ns)
    motor1_planner.set_planner(planner)

    data = []

    try:
        for i in range(25000 * 4):
            tt, *pos, vel = motor1.cur_state  # pylint: disable=unpacking-non-sequence
            data.append((tt, sum(pos), vel, motor1_controller.real_pos))

        # print(motor1.cur_state)
        print(events.wait())
        print(motor1.cur_state)
    finally:
        updater.stop()

    start_time = data[0][0]
    ttime = [(i[0] - start_time) / 1e9 for i in data]
    plt.plot(ttime, [i[1] for i in data], color="blue")
    plt.plot(ttime, [i[2] / 100 for i in data], color="green")
    plt.plot(ttime, [i[3] for i in data], color="darkblue")
    plt.show()


if __name__ == "__main__":
    main()
