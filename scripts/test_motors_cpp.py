import time

import matplotlib.pyplot as plt
from loguru import logger

from raspberry_pcb.motors_cpp import motors_cpp


@logger.catch
def main() -> None:
    gpio = motors_cpp.RaspberryGPIO()
    motor1_controller = motors_cpp.SoftwareStepMotorGPIOController(gpio, 3, 27, False)
    motor1 = motors_cpp.SoftwareStepMotor(motor1_controller, 21696, 0)

    updater = motors_cpp.Updater()
    updater.add(motor1)
    updater.start(100000, True, True)

    motor1.set_target_pos_vel((0, 0), 21696)

    data = []

    try:
        for i in range(2500):
            if 100 < i < 2000:
                motor1.set_target_pos_vel((0, 0), 21696 - 21696 * 2 * ((i // 400) % 2))
            t1 = time.monotonic_ns()
            res = (tuple(motor1.cur_state)[1:2], motor1_controller.real_pos)
            t2 = time.monotonic_ns()
            data.append(((t1 + t2) / 2, *res))
    finally:
        updater.stop()

    start_time = data[0][0]
    ttime = [(i[0] - start_time) / 1e9 for i in data]
    plt.plot(ttime, [sum(i[1]) for i in data], color="blue")
    plt.plot(ttime, [i[2] for i in data], color="green")
    plt.show()


if __name__ == "__main__":
    main()
