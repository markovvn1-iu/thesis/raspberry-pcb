from loguru import logger

from raspberry_pcb.motors_cpp import cpp_events, cpp_gpio, cpp_updater


@logger.catch
def main() -> None:
    gpio = cpp_gpio.RaspberryGPIO()

    event_handler = cpp_events.EventHandler()
    switch1_event = cpp_events.GPIOSwitchEventNotifier(event_handler, gpio, 5)  # , 100_000_000)
    switch2_event = cpp_events.GPIOSwitchEventNotifier(event_handler, gpio, 6)  # , 100_000_000)
    switch3_event = cpp_events.GPIOSwitchEventNotifier(event_handler, gpio, 13)  # , 100_000_000)

    updater = cpp_updater.Updater()
    updater.add(switch1_event)
    updater.add(switch2_event)
    updater.add(switch3_event)
    updater.start(0)

    while True:
        e = event_handler.wait(0.1)
        if e is None:
            continue
        print(e)


if __name__ == "__main__":
    main()
