import time

from loguru import logger

from raspberry_pcb.controllers import Nema17GPIOMotor
from raspberry_pcb.motors_cpp.gpio import RaspberryGPIO
from raspberry_pcb.motors_cpp.updater import Updater
from raspberry_pcb.planners import SpeedPlanner, StopPlanner


@logger.catch
def main() -> None:
    gpio = RaspberryGPIO()
    gpio.pinMode(10, RaspberryGPIO.OUTPUT)
    motor1 = Nema17GPIOMotor(gpio, 3, 27)
    updater = Updater()
    updater.add(motor1)
    updater.start(0)

    gpio.digitalWrite(10, 0)

    state = motor1.cur_state
    motor1.set_planner(SpeedPlanner(state.vel, 10000, 5, motor1.max_accel).compile(state.timestamp_ns, state.pos))
    time.sleep(5)
    state = motor1.cur_state
    motor1.set_planner(StopPlanner(state.vel, motor1.max_accel).compile(state.timestamp_ns, state.pos))

    time.sleep(1)

    updater.stop()

    gpio.digitalWrite(10, 1)


if __name__ == "__main__":
    main()
