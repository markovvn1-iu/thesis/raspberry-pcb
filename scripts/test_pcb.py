from raspberry_pcb.pcb import RaspberryPCB

pcb = RaspberryPCB()
pcb.start()

pcb.enable(True)
pcb.calibrate().result()

sh_y = 0.03
pcb.p2p((0, sh_y + 0), (0, 0)).result()


def go() -> None:
    try:
        pcb.p2p((0, sh_y + 0), (0, 0.2)).result()
        pcb.const_vel((0, 0.2), 0.7).result()
        pcb.p2p((0.02, sh_y + 0.14), (0, -0.2)).result()
        pcb.const_vel((0, -0.2), 0.7).result()
        pcb.p2p((0.04, sh_y + 0), (0, 0.2)).result()
        pcb.const_vel((0, 0.2), 0.7).result()
        pcb.p2p((0.06, sh_y + 0.14), (0, -0.2)).result()
        pcb.const_vel((0, -0.2), 0.7).result()
        pcb.p2p((0.08, sh_y + 0), (0, 0.2)).result()
        pcb.const_vel((0, 0.2), 0.7).result()
        pcb.p2p((0.10, sh_y + 0.14), (0, -0.2)).result()
        pcb.const_vel((0, -0.2), 0.7).result()
        pcb.p2p((0, sh_y + 0), (0, 0)).result()
    finally:
        pcb.stop_motors()


pcb.enable(False)
pcb.stop()
