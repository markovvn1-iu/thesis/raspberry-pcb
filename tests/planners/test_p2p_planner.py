from typing import Optional

import pytest

from raspberry_pcb.planners.p2p_planner import P2PPlanner, Planner

EPSILON = 1e-8


@pytest.mark.parametrize("inverse", [False, True])
@pytest.mark.parametrize("add_time", [None, 0, 1, 2, 5, 10, 50, 200, 1000])
@pytest.mark.parametrize("v_max_k", [1.1, 1.5, 2, 5, 10, 200, 1.0])
@pytest.mark.parametrize(
    "v_start,p_end,v_end,max_accel",
    [
        # Simple cases
        (0, 0, 0, 10),  # 17
        (0, 10, 0, 100),  # 1
        (0, 1, 0, 1),  # 1
        (0, -5, 0, 1),  # 1
        (5, 1, 5, 10),  # 2
        (5, 4, 2, 10),  # 2
        # Cases 6
        (10, 5, 5, 5),  # 6
        (10, 7.49, 5, 5),  # 6
        (10, 7.51, 5, 5),  # 2
        (10, 9, 5, 5),  # 2
        # Cases 7, 12
        (5, 2, -5, 10),  # 4
        (5, 2, -10, 10),  # 8
        (5, 2, -10, 1),  # 8
        (5, 0, -5, 10),  # 3
        (5, -0.01, -5, 10),  # 7
        (5, -1, -5, 10),  # 7
        (5, -1, 5, 10),  # 12
        (1, -1, 5, 10),  # 12
        (1, -1, 0, 10),  # 18
        (0, -1, 0, 10),  # 1
        (10, -1, -8, 10),  # 7
        (10, -1, -10.954, 10),  # 7
        (10, -1, -10.955, 10),  # 16
        (10, -1, -12, 10),  # 16
        # Case 10
        (5, 9, 10, 5),  # 2
        (5, 7.51, 10, 5),  # 2
        (5, 7.49, 10, 5),  # 10
        (5, 6, 10, 5),  # 10
        # Case 11
        (0, 6, 10, 10),  # 14
        (0, 5.01, 10, 10),  # 14
        (0, 4.99, 10, 10),  # 11
        (0, 1, 10, 10),  # 11
        # Case 13
        (10, 10, 0, 10),  # 5
        (10, 5.01, 0, 10),  # 5
        (10, 4.99, 0, 10),  # 13
        (10, 3, 0, 10),  # 13
        # Case 15
        (10, 1, -12, 10),  # 4
        (10, 1, -8.945, 10),  # 4
        (10, 1, -8.944, 10),  # 15
        (10, 1, -2, 10),  # 15
    ],
)
def test_p2p_planner(  # pylint: disable=too-many-arguments
    v_start: float,
    p_end: float,
    v_end: float,
    max_accel: float,
    v_max_k: float,
    add_time: Optional[float],
    inverse: bool,
) -> None:
    if inverse:
        v_start, p_end, v_end = -v_start, -p_end, -v_end

    max_vel = max(abs(v_start), abs(v_end), 0.1) * v_max_k
    p = P2PPlanner(v_start, p_end, v_end, max_vel, max_accel)

    p_: Planner = p
    if add_time is not None:
        target_duration = p.info.duration + add_time
        p_ = p.add_time(add_time)
        assert abs(p_.info.duration - target_duration) < EPSILON

    assert abs(p_.info.end_vel - v_end) < EPSILON
    assert abs(p_.info.start_vel - v_start) < EPSILON
    assert abs(p_.info.delta_pos - p_end) < EPSILON


@pytest.mark.parametrize(
    "v_start,p_end,v_end,max_vel,max_accel",
    [
        (58800.0, 2992.1944370995006, 0.0, 58800.0, 675000),
    ],
)
def test_p2p_planner_special(v_start: float, p_end: float, v_end: float, max_vel: float, max_accel: float) -> None:
    p = P2PPlanner(v_start, p_end, v_end, max_vel, max_accel)
    assert abs(p.info.end_vel - v_end) < EPSILON
    assert abs(p.info.start_vel - v_start) < EPSILON
    assert abs(p.info.delta_pos - p_end) < EPSILON
