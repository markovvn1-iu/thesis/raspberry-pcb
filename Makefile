VENV ?= .venv
CODE = tests scripts raspberry_pcb

.PHONY: venv
venv:
	python -m venv $(VENV)
	$(VENV)/bin/python -m pip install --upgrade pip
	$(VENV)/bin/python -m pip install poetry
	$(VENV)/bin/poetry build
	$(VENV)/bin/poetry install

.PHONY: test
test:
	$(VENV)/bin/pytest -v tests


.PHONY: lint
lint:

	$(VENV)/bin/poetry lock --check
	# python linters
	$(VENV)/bin/pflake8 --jobs 4 --statistics --show-source $(CODE)
	$(VENV)/bin/pylint --recursive=y --jobs 4 $(CODE)
	$(VENV)/bin/mypy --show-error-codes $(CODE)
	$(VENV)/bin/black --skip-string-normalization --check $(CODE)

.PHONY: format
format:
	$(VENV)/bin/isort $(CODE)
	$(VENV)/bin/black $(CODE)
	$(VENV)/bin/pautoflake --recursive --remove-all-unused-imports --in-place $(CODE)


.PHONY: ci
ci:	lint test


.PHONY: build
build:
	[ -f setup.py ] && rm setup.py || true
	$(VENV)/bin/poetry build
	$(VENV)/bin/poetry install


.PHONY: up
up:
	$(VENV)/bin/python -m remote_pcb /dev/ttyACM0,/dev/ttyACM1,/dev/ttyACM2


.PHONY: install_venv
install_venv:
	python -m venv $(VENV)
	$(VENV)/bin/python -m pip install --upgrade pip
	$(VENV)/bin/python -m pip install poetry
	$(VENV)/bin/poetry build
	$(VENV)/bin/poetry install --only main