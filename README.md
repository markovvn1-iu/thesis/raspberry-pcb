<div align="center">
  <img src=".gitlab/logo.svg" height="124px"/><br/>
  <h1>Raspberry PCB</h1>
  <p>Python server and client for remote PCB control</a></p>
</div>



## 📝 About The Project

This repository contains a python package with server for raspberry and client (SDK) for the remote PCB control

## ⚡️ Quick start

Install package using *pip* package manager:

```bash
pip install --index-url https://gitlab.com/api/v4/groups/63088747/-/packages/pypi/simple raspberry_pcb_sdk
```

### :star: Poetry-way to quick start

To install this package with *poetry* you have to add the new source to your `pyproject.toml` file:

```bash
[[tool.poetry.source]]
name = "project"
url = "https://gitlab.com/api/v4/groups/63088747/-/packages/pypi/simple"
```

You can then add the package as a dependency:

```bash
poetry add --source project raspberry_pcb_sdk
```

### :rocket: Run Remote PCB server

Download the repository and change the current directory:

```bash
git clone git@gitlab.com:markovvn1-iu/thesis/raspberry-pcb.git && cd raspberry-pcb
```

Configure virtual environment. Make sure a `.venv` folder has been created after this step.

```bash
make venv
# source .venv/bin/activate
```

Run the server:

```bash
make up
```

### ⚙️ Developing

Go to `scripts` folder and run some examples.

Build C++ code:

```bash
make build
```

Run linters, formaters and tests:
```bash
make lint  # check code quality
make format  # beautify code
make test  # run tests
```

## :computer: Contributors

<p>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>Markovvn1@gmail.com</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
</p>